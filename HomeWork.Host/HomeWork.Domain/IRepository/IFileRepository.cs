﻿using System.Threading.Tasks;

namespace HomeWork.Domain.IRepository
{
    public interface IFileRepository
    {
        Task WriteToFile(string row);
        Task<string> ReadFromLastFile(bool benchmarksResult);
    }
}
