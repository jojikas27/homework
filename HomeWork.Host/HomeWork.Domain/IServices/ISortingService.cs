﻿using System.Threading.Tasks;

namespace HomeWork.Domain.IServices
{
    public interface ISortingService
    {
        Task<string> LatestResults(bool benchmarksResult);
        Task SortBenchmarkAndSave(int[] numbers, int multiply);
        Task<string> DoSort(int[] numbers, int iterations, bool? onlyResult = false);
    }
}
