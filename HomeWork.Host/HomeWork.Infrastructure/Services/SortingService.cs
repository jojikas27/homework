﻿using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWork.Domain.IRepository;
using HomeWork.Domain.IServices;
using HomeWork.Infrastructure.Extensions;

namespace HomeWork.Infrastructure.Services
{
    public class SortingService : ISortingService
    {
        private readonly IFileRepository _fileRepository;
        public SortingService(IFileRepository fileRepository)
        {
            _fileRepository = fileRepository;
        }

        public async Task<string> LatestResults(bool benchmarksResult)
        {
            return await _fileRepository.ReadFromLastFile(benchmarksResult).ConfigureAwait(false);
        }
        
        public async Task SortBenchmarkAndSave(int[] numbers,int iterations)
        {
            var result = await DoSort(numbers, iterations);

            await _fileRepository.WriteToFile(result).ConfigureAwait(false);
        }
        
        public async Task<string>DoSort(int[] numbers, int iterations, bool? onlyResult = false)
        {
            var tasks = new ConcurrentBag<Task<BenchmarkHolder>>();

            //Default
            ((int[])numbers.Clone()).DefaultSort(tasks, iterations);

            //Insertion
            ((int[])numbers.Clone()).InsertionSort(tasks, iterations);

            //Bubble
            ((int[])numbers.Clone()).BubbleSort(tasks, iterations);

            Task.WaitAll(tasks.ToArray());
            var sb = new StringBuilder();
            //First line to meet task requirements 
            var fastest = tasks.OrderBy(x => x.Result.Duration).FirstOrDefault();
            sb.AppendLine(fastest?.Result.Result);
            if (onlyResult.HasValue && !onlyResult.Value)
            {
                foreach (var task in tasks)
                {
                    sb.AppendLine(
                        $"Algorithm:{task.Result.Algorithm} Result:{task.Result.Result} Time:{task.Result.Duration}ms.");
                }
            }

            return await Task.FromResult(sb.ToString());
        }
    }
    public class BenchmarkHolder
    {
        public string Algorithm { get; set; }
        public long Duration { get; set; }
        public string Result { get; set; }
    }
}
