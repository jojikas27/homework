﻿using System;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using HomeWork.Domain.IRepository;

namespace HomeWork.Infrastructure.Repository
{
    public class FileRepository : IFileRepository
    {
        private readonly string _location;

        public FileRepository()
        {
            _location = Path.Combine(AppContext.BaseDirectory, "Results");
            CheckDirectory();
        }
        public async Task WriteToFile(string row)
        {
            CheckDirectory();
            CleanOldFiles();
            await using StreamWriter outputFile =
                new StreamWriter(Path.Combine(_location, $"file{DateTime.Now:yyyyMMddHHmmssfff}.txt"));
            await outputFile.WriteAsync(row);
        }

        public async Task<string> ReadFromLastFile(bool benchmarksResult)
        {
            CheckDirectory();
            var directory = new DirectoryInfo(_location);
            var latestFile = directory.GetFiles().OrderByDescending(x => x.LastWriteTime).FirstOrDefault();
            if (latestFile == null) return await Task.FromResult("No previous records");
            using StreamReader streamReader = new StreamReader(latestFile.FullName);
            if (benchmarksResult)
            {
                await streamReader.ReadLineAsync(); //Ignore first line
                return await streamReader.ReadToEndAsync();
            }

            return await streamReader.ReadLineAsync();
        }

        private void CleanOldFiles()
        {
            //Delete older then 5minutes
            string[] files = Directory.GetFiles(_location);
            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                if (fi.LastAccessTime < DateTime.Now.AddMinutes(-5))
                    fi.Delete();
            }
        }

        private void CheckDirectory()
        {
            if (!Directory.Exists(_location))
                Directory.CreateDirectory(_location);
        }
    }
}
