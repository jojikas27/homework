﻿using System;
using System.Collections.Concurrent;
using System.Diagnostics;
using System.Threading.Tasks;
using HomeWork.Infrastructure.Services;

namespace HomeWork.Infrastructure.Extensions
{
    public static class ArrayExtensions
    {
        public static void DefaultSort(this int[] array, ConcurrentBag<Task<BenchmarkHolder>> tasks, int iterations)
        {
            tasks.Add(Task<BenchmarkHolder>.Factory.StartNew(() =>
            {
                var result = "";
                var sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < iterations; i++)
                {
                    var newArray = (int[])array.Clone();
                    Array.Sort(newArray);
                    if (i == 0)
                        result = string.Join(' ', newArray);
                }
                sw.Stop();
                return new BenchmarkHolder
                {
                    Algorithm = "Default",
                    Duration = sw.ElapsedMilliseconds,
                    Result = result
                };
            }));
        }

        public static void InsertionSort(this int[] array, ConcurrentBag<Task<BenchmarkHolder>> tasks, int iterations)
        {
            tasks.Add(Task<BenchmarkHolder>.Factory.StartNew(() =>
            {
                var result = "";
                var sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < iterations; i++)
                {
                    var newArray = (int[]) array.Clone();
                    for (int j = 0; j < newArray.Length; j++)
                    {
                        var item = newArray[j];
                        var currentIndex = j;

                        while (currentIndex > 0 && newArray[currentIndex - 1] > item)
                        {
                            newArray[currentIndex] = newArray[currentIndex - 1];
                            currentIndex--;
                        }

                        newArray[currentIndex] = item;
                    }
                    if (i == 0)
                        result = string.Join(' ', newArray);
                }
                sw.Stop();
                return new BenchmarkHolder
                {
                    Algorithm = "InsertionSort",
                    Duration = sw.ElapsedMilliseconds,
                    Result = result
                };
            }));
        }

        public static void BubbleSort(this int[] array, ConcurrentBag<Task<BenchmarkHolder>> tasks, int iterations)
        {
            tasks.Add(Task<BenchmarkHolder>.Factory.StartNew(() =>
            {
                var result = "";
                var sw = new Stopwatch();
                sw.Start();
                for (int i = 0; i < iterations; i++)
                {
                    var newArray = (int[]) array.Clone();
                    var itemMoved = true;
                    while (itemMoved)
                    {
                        itemMoved = false;
                        for (int j = 0; j < newArray.Length - 1; j++)
                        {
                            if (newArray[j] > newArray[j + 1])
                            {
                                var lowerValue = newArray[j + 1];
                                newArray[j + 1] = newArray[j];
                                newArray[j] = lowerValue;
                                itemMoved = true;
                            }
                        }
                    }
                    if (i == 0)
                        result = string.Join(' ', newArray);
                }
                sw.Stop();
                return new BenchmarkHolder
                {
                    Algorithm = "BubbleSort",
                    Duration = sw.ElapsedMilliseconds,
                    Result = result
                };
            }));
        }
    }
}
