﻿using HomeWork.Host.Models.Requests;
using Swashbuckle.AspNetCore.Filters;

namespace HomeWork.Host.Examples
{
    public class SortControllerExamples : IExamplesProvider<SortRequest>
    {
        SortRequest IExamplesProvider<SortRequest>.GetExamples()
        {
            return new SortRequest("1 2 8 2 10",1);
        }
    }
}
