using System;
using System.IO;
using System.Reflection;
using HomeWork.Host.Configurations;
using HomeWork.Host.Models.Requests;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Filters;

namespace HomeWork.Host
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddApplicationInsightsTelemetry(); <= lets pretend that all logs are going to Azure AI
            services.AddSwaggerGen(c =>
            {
                // Set the comments path for the Swagger JSON and UI.
                var xmlPath = Path.Combine(AppContext.BaseDirectory, "API.xml");
                c.IncludeXmlComments(xmlPath);
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Danske homework task",
                    Description = "API: Number ordering solution",
                    Contact = new OpenApiContact
                    {
                        Name = "Mantas Parfionovas",
                        Email = "mantas.parfionovas@gmail.com",
                        Url = new Uri("https://www.linkedin.com/in/mantas-parfionovas/"),
                    }
                });
                c.ExampleFilters();
            });
            services.AddSwaggerExamplesFromAssemblyOf<SortRequest>();
            services.ConfigureDependencyInjections();
            services.AddControllers();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "API V1");
                c.RoutePrefix = string.Empty;
                c.DisplayRequestDuration();
                c.DefaultModelsExpandDepth(-1);
            });
            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
