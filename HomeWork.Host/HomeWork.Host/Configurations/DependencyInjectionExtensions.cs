﻿using HomeWork.Domain.IRepository;
using HomeWork.Domain.IServices;
using HomeWork.Infrastructure.Repository;
using HomeWork.Infrastructure.Services;
using Microsoft.Extensions.DependencyInjection;

namespace HomeWork.Host.Configurations
{
    /// <summary>
    /// Register here all dependency injections (DI)
    /// </summary>
    public static class DependencyInjectionExtensions
    {
        public static void ConfigureDependencyInjections(this IServiceCollection services)
        {
            services.AddScoped<IFileRepository, FileRepository>()
                .AddScoped<ISortingService, SortingService>();
        }
    }
}
