﻿using System;
using HomeWork.Host.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeWork.Host.Controllers
{
    [Produces("application/json")] //produce only JSON responses
    [Consumes("application/json")] //accept only JSON requests
    public abstract class ControllerBase : Microsoft.AspNetCore.Mvc.ControllerBase
    {
        protected readonly ILogger<ControllerBase> Logger;
        
        protected ControllerBase(ILogger<ControllerBase> logger)
        {
            Logger = logger;
        }

        [NonAction]
        protected ObjectResult OkNoContent()
        {
            return StatusCode(StatusCodes.Status204NoContent, null);
        }

        [NonAction]
        protected ObjectResult InternalServerError(string error)
        {
            Logger.LogError(error);
            return StatusCode(StatusCodes.Status500InternalServerError, Error(error));
        }

        private static ErrorResponse Error(string error)
        {
            return new ErrorResponse
            {
                Message = error,
                TimeStamp = DateTimeOffset.Now
            };
        }
    }
}
