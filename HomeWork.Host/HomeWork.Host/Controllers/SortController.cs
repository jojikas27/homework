﻿using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using HomeWork.Domain.IServices;
using HomeWork.Host.Models;
using HomeWork.Host.Models.Requests;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HomeWork.Host.Controllers
{
    [ApiController]
    [Route("api/v1/sort")]
    public class SortController : ControllerBase
    {
        private readonly ISortingService _sortingService;

        public SortController(ILogger<SortController> logger, ISortingService sortingService) : base(logger)
        {
            _sortingService = sortingService;
        }

        [HttpPost("sort")]
        [ProducesResponseType((int)HttpStatusCode.NoContent)]
        [ProducesResponseType((int)HttpStatusCode.BadRequest)]
        [ProducesResponseType((int)HttpStatusCode.InternalServerError, Type = typeof(ErrorResponse))]
        public async Task<IActionResult> Sort([FromBody]SortRequest sortRequest)
        {
            try
            {
                var validationResponse = Validate(sortRequest.LineOfNumbers);
                await _sortingService.SortBenchmarkAndSave(validationResponse,sortRequest.Iterations).ConfigureAwait(false);
                return OkNoContent();
            }
            catch (Exception e)
            {
                return InternalServerError(e.Message);
            }
        }

        [HttpGet("getLatest")]
        public async Task<IActionResult> GetLatest([FromQuery]bool benchmarkResults = false)
        {
            try
            {
                var result = await _sortingService.LatestResults(benchmarkResults).ConfigureAwait(false);
                return Ok(result);
            }
            catch (Exception e)
            {
                return InternalServerError(e.Message);
            }
        }

        private int[] Validate(string lineOfNumbers)
        {
            int[] arrayOfNumbers = lineOfNumbers.Split(' ').Select(n => Convert.ToInt32(n)).ToArray();
            if (arrayOfNumbers.All(x => x > 0 && x < 11))
            {
                return arrayOfNumbers;
            }

            throw new Exception("Validation: accepting only numbers 1 - 10");
        }
    }
}
