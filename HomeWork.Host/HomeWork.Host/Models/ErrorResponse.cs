﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HomeWork.Host.Models
{
    public class ErrorResponse
    {
        /// <summary>
        /// Error message
        /// </summary>
        /// <example>This is an error message</example>
        [Required]
        public string Message { get; set; }
        public DateTimeOffset TimeStamp { get; set; }
        //Additional information for User
    }
}
