﻿using System.ComponentModel.DataAnnotations;

namespace HomeWork.Host.Models.Requests
{
    public class SortRequest
    {
        public SortRequest()
        {
            
        }
        public SortRequest(string lineOfNumbers, int iterations)
        {
            LineOfNumbers = lineOfNumbers;
            Iterations = iterations;
        }
        [Required]
        public string LineOfNumbers { get; set; }

        [Required]
        [Range(1, int.MaxValue)]
        public int Iterations { get; set; }
    }
}
