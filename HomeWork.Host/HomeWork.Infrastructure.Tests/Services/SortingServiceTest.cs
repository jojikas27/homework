﻿using System.Threading.Tasks;
using HomeWork.Domain.IRepository;
using HomeWork.Infrastructure.Services;
using Moq;
using NUnit.Framework;

namespace HomeWork.Infrastructure.Tests.Services
{
    [TestFixture()]
    public class SortingServiceTest
    {
        private Mock<IFileRepository> _fileRepositoryMock;
        private SortingService _sortingService;
        [SetUp]
        public void SetUp()
        {
            _fileRepositoryMock = new Mock<IFileRepository>();
            _fileRepositoryMock.Setup(s => s.WriteToFile(It.IsAny<string>()));
            _fileRepositoryMock.Setup(s => s.ReadFromLastFile(It.IsAny<bool>()))
                .Returns(Task.FromResult("1 2 3 4 5"));
            _sortingService = new SortingService(_fileRepositoryMock.Object);
        }

        [Test]
        public async Task LatestResultsCalledOnceTest()
        {
            await _sortingService.LatestResults(It.IsAny<bool>());
           _fileRepositoryMock.Verify(x=>x.ReadFromLastFile(It.IsAny<bool>()),Times.Once);
        }

        [Test]
        [TestCase(new[]{5, 2, 8, 10, 1}, ExpectedResult = "1 2 5 8 10\r\n")]
        public async Task<string> SortOnlyResultTest(int[] numbers)
        {
            return await _sortingService.DoSort(numbers, 1, true);
        }

        [Test]
        public async Task WriteToFileCalledOnceTest()
        {
            await _sortingService.SortBenchmarkAndSave(new []{1,2,3,4,5},1);
            _fileRepositoryMock.Verify(x=>x.WriteToFile(It.IsAny<string>()),Times.Once());
        }
    }
}
