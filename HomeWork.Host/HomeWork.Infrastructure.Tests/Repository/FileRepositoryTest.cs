﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HomeWork.Infrastructure.Repository;
using NUnit.Framework;


namespace HomeWork.Infrastructure.Tests.Repository
{
    [TestFixture]
    public class FileRepositoryTest
    {
        private FileRepository _fileRepository;
        private string _location;
        [SetUp]
        public void SetUp()
        {
            _fileRepository = new FileRepository();
            _location = Path.Combine(AppContext.BaseDirectory, "Results");
            if (!Directory.Exists(_location))
                Directory.CreateDirectory(_location);
            string[] files = Directory.GetFiles(_location);
            foreach (string file in files)
            {
                FileInfo fi = new FileInfo(file);
                fi.Delete();
            }
        }

        [Test]
        public async Task CombinedWriteReadTest()
        {
            await _fileRepository.WriteToFile("Test");
            var result = await _fileRepository.ReadFromLastFile(false);
            Assert.AreEqual("Test",result);
        }

        [Test]
        public async Task WriteToFileTest()
        {
            var directory = new DirectoryInfo(_location);
            var fileCountBefore = directory.GetFiles().Count();
            await _fileRepository.WriteToFile("Test");
            var fileCountAfter = directory.GetFiles().Count();
            Assert.AreEqual(fileCountBefore+1,fileCountAfter);
        }

        [Test]
        [TestCase(false,ExpectedResult = "1 2 3 4 5")]
        [TestCase(true,ExpectedResult = "Algorithm:Bubble\r\n")]
        public async Task<string> ReadFromLastFileTest(bool benchmarksResult)
        {
            var sb = new StringBuilder();
            sb.AppendLine("1 2 3 4 5");
            sb.AppendLine("Algorithm:Bubble");
            await _fileRepository.WriteToFile(sb.ToString());
            var result = _fileRepository.ReadFromLastFile(benchmarksResult);
            Debug.WriteLine(result);
            return result.Result;
        }
    }
}
